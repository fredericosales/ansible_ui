# var
VIRTUALENV_LAZY=$(which virtualenvwrapper_lazy.sh)
VIRTUALENVWRAPPER_SCRIPT=$(which virtualenvwrapper.sh)
PROJECT_HOME="${HOME}"/projects
WORKON_HOME="${HOME}"/.venvs

# exports
export PATH="${HOME}/bin:${PATH}"
export PATH="${HOME}/.local/bin:${PATH}"
export PATH=/usr/local/bin:"${PATH}"
export LC_ALL=C
export EDITOR=nvim
export "${WORKON_HOME}"
export "${PROJECT_HOME}"
export "${VIRTUALENVWRAPPER_SCRIPT}"

# source
source $VIRTUALENV_LAZY

# functions


# aliases
alias search='doas find $1 -name $2 2>/dev/null'
alias grep='grep --color'
alias egrep='egrep --color'
alias fgrep='fgrep --color'
alias tshark='tshark --color'
alias genpwd='head -c32 /dev/urandom | base64'
alias scan='doas nmap -sS -P0 -O'
alias v='doas vim'